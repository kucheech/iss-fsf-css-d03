## create project (can use VS Code to do this)
1. ```mkdir <project name>```
1. ```cd <project name>```
1. ```mkdir server```
1. ```mkdir client```
1. cd server and create app.js

## setup git repo in bitbucket
1. add new git repo at bitbucket
1. copy the git remote add origin command

## prepare git (can do this in VS Code terminal)
1. ```git init```
1. ```git remote add origin https://kucheech@bitbucket.org/kucheech/iss-fsf-css-d03.git```

## setup package.json
1. ```npm init --yes```
1. to edit the entry point accordingly, for example to server/app.js

## install express
1. ```npm install --save express```

## git commit
1. create a .gitignore and add "node_modules" to it
1. ```git add .```
1. ```git commit -m "initial commit"```
1. ```git push -u origin master```

# to run app
1. can use nodemon
1. alternatively, execute ```node server/app.js```
1. start browser and view app at http://localhost:3000

# setup bower
1. create a .bowerrc file and configure the directory
```{
    "directory": "client/bower_components"
}```
1. bower install angular --save
1. bower install bootstrap --save