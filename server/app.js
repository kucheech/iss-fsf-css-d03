console.log("D03");
const express = require("express");
const app = express();
console.log(__dirname);
const NODE_PORT = process.env.NODE_PORT || 3000;

const url = __dirname + "/../client";

if(!NODE_PORT) {
    console.log("express port is not set");
    process.exit();
}

app.use(express.static(url));
// const url2 = __dirname + "/../client/bower_components";
// app.use("/bower_components", express.static(url2));

app.listen(NODE_PORT, function () {
    console.log("Web app started at port " + NODE_PORT);
});